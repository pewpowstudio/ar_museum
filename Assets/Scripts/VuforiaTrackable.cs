﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class VuforiaTrackable : DefaultTrackableEventHandler
{
    public event Action OnFoundItem = delegate { };
    public event Action OnLostItem = delegate { };

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();

        OnFoundItem.Invoke();
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();

        OnLostItem.Invoke();
    }
}
