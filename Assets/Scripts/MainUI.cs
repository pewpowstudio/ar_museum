﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : MonoBehaviour
{
    public GameObject startWindow;

    public GameObject winnerAlert;
    
    public GameObject quest1Button;
    public GameObject quest2Button;

    public GameObject quest1Window;
    public GameObject vuforia1Window;
    public GameObject image1Found;
    
    public GameObject quest2Window;
    public GameObject vuforia2Window;
    public GameObject image2Found;

    public GameObject ratingWindow;
    public GameObject achivmentWindow;

    public GameObject vuforial;

    public VuforiaTrackable image1Trackable;
    public VuforiaTrackable image2Trackable;

    public Text currectUserScore;

    public bool quest1Active = true;
    public bool quest2Active = false;
    public bool winner = false;

    float score = 0;

    void Update()
    {
        currectUserScore.text = $"{score}";

        quest1Button.SetActive(quest1Active);
        quest2Button.SetActive(quest2Active);

        winnerAlert.SetActive(winner);
    }

    void OnEnable()
    {
        image1Trackable.OnFoundItem += OnImage1Found;
        image1Trackable.OnLostItem += OnImage1Lost;

        image2Trackable.OnFoundItem += OnImage2Found;
        image2Trackable.OnLostItem += OnImage2Lost;
    }

    void OnDisable()
    {
        image1Trackable.OnFoundItem -= OnImage1Found;
        image1Trackable.OnLostItem -= OnImage1Lost;

        image2Trackable.OnFoundItem -= OnImage2Found;
        image2Trackable.OnLostItem -= OnImage2Lost;
    }

    public void UI_Quit()
    {
        Application.Quit();
    }

    public void UI_Quest1Window()
    {
        quest1Window.SetActive(true);
    }

    public void UI_Quest1WindowBack()
    {
        quest1Window.SetActive(false);
    }

    public void UI_Vuforia1Window()
    {
        startWindow.SetActive(false);
 
        quest1Window.SetActive(false);

        image1Found.SetActive(false);
        vuforia1Window.SetActive(true);
        //image1Trackable.gameObject.SetActive(true);

        vuforial.SetActive(true);

        //image2Trackable.gameObject.SetActive(false);
    }

    public void UI_Vuforia1WindowBack()
    {
        image1Found.SetActive(false);
        vuforia1Window.SetActive(false);
        //image1Trackable.gameObject.SetActive(false);

        vuforial.SetActive(false);

        startWindow.SetActive(true);
    }

    public void UI_Quest2Window()
    {
        quest2Window.SetActive(true);
    }

    public void UI_Quest2WindowBack()
    {
        quest2Window.SetActive(false);
    }

    public void UI_Vuforia2Window()
    {
        startWindow.SetActive(false);
 
        quest2Window.SetActive(false);

        image2Found.SetActive(false);
        vuforia2Window.SetActive(true);
        //image2Trackable.gameObject.SetActive(true);

        vuforial.SetActive(true);

        //image1Trackable.gameObject.SetActive(false);
    }

    public void UI_Vuforia2WindowBack()
    {
        image2Found.SetActive(false);
        vuforia2Window.SetActive(false);
        //image2Trackable.gameObject.SetActive(false);

        vuforial.SetActive(false);

        startWindow.SetActive(true);
    }

    public void UI_RatingWindow()
    {
        ratingWindow.SetActive(true);
    }

    public void UI_RatingWindowBack()
    {
        ratingWindow.SetActive(false);
    }

    public void UI_AchivementsWindow()
    {
        achivmentWindow.SetActive(true);
    }

    public void UI_AchivementsWindowBack()
    {
        achivmentWindow.SetActive(false);
    }

    void OnImage1Found()
    {
        image1Found.SetActive(true);

        score += 1;

        if (quest1Active)
        {
            quest1Active = false;
            quest2Active = true;
        }
    }

    void OnImage1Lost()
    {
        image1Found.SetActive(false);
    }

    void OnImage2Found()
    {
        image2Found.SetActive(true);

        score += 1;

        if (quest2Active)
        {
            quest2Active = false;
            quest1Active = true;

            winner = true;
        }
    }

    void OnImage2Lost()
    {
        image2Found.SetActive(false);
    }
}
